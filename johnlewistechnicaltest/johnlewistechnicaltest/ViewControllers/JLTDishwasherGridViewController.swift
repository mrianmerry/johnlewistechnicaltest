//
//  JLTDishwasherGridViewController.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 07/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//

import UIKit
import CoreData

class JLTDishwasherGridViewController: UIViewController {
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 1.0, left: 1.0, bottom: 1.0, right: 1.0)
    fileprivate let reuseIdentifier = "dishwasherGridCollectionViewCell"
    fileprivate let dishwasherGridURL = "https://api.johnlewis.com/v1/products/search?q=dishwasher&key=Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb&pageSize=20"
    
    fileprivate var gridImages = [UIImage]()
    fileprivate var dishwashers = [JLTDishwasher]()
    fileprivate var managedContext: NSManagedObjectContext? = nil
    
    @IBOutlet weak var dishwasherGridCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dishwasherGridCollectionView.register(UINib.init(nibName: "JLTDishwasherGridCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        self.navigationItem.title = "Dishwashers"
        self.navigationItem.backBarButtonItem?.title = ""
        
        loadDishwashersFromCoreData()
        
        if (dishwashers.count == 0) {
            loadDishwashersFromAPI()
        }
        else {
            loadDishwasherImages()
        }
        
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            self.forceCollectionViewLayout()
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
        
    }
    
    func forceCollectionViewLayout() {
        dishwasherGridCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "displayDishwasherProductInformation") {
            
            guard let dishwasher = sender as? JLTDishwasher else {
                print("error!")
                return
            }
            guard let productInformation = segue.destination as? JLTProductInformationViewController else {
                print ("error")
                return
            }
            
            productInformation.dishwasher = dishwasher
        }
    }

}

extension JLTDishwasherGridViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return dishwashers.count;
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier,
                                                      for: indexPath) as! JLTDishwasherGridCollectionViewCell
        
        let dishwasher = dishwashers[indexPath.row]
        
        cell.dishwasherTitleLabel.text = dishwasher.title
        cell.dishwasherPriceLabel.text = "£" + dishwasher.price!
        
        if gridImages.count >  indexPath.row {
            cell.dishwasherImageView.image = gridImages[indexPath.row]
        }
        
        return cell
        
    }
}

extension JLTDishwasherGridViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dishwasher = dishwashers[indexPath.row]
        
        performSegue(withIdentifier: "displayDishwasherProductInformation", sender: dishwasher)
    }
}

extension JLTDishwasherGridViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let itemsPerRow: CGFloat = 4.0
        
        let paddingSpace = sectionInsets.left * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem + 100)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

extension JLTDishwasherGridViewController {
    
    func loadDishwashersFromCoreData() {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        if (managedContext == nil) {
            managedContext = appDelegate.persistentContainer.viewContext
        }
        
        do {
            dishwashers = try managedContext!.fetch(JLTDishwasher.fetchRequest())
        } catch let error as NSError {
            print("Could not fetch.\n    Error Description: \"\(error.localizedDescription)\"\n    Error Info: \"\(error.userInfo)\"\n    Error Object: \"\(error)\"")
        }
    }
    
    func loadDishwashersFromAPI() {
        
        
        URLSession.createAPIRequest(with: dishwasherGridURL) { (responseData) in
            self.createDishwashers(with: responseData)
            self.loadDishwasherImages()
        }
    }
    
    func createDishwashers(with data:Data) {
        do {
            
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            
            guard let products = json?["products"] as? [AnyObject] else {
                print("error trying to access products from api response")
                return
            }
            
            for product in products {                
                
                guard let productID = product["productId"] as? String else {
                    print("error trying to retrieve product id of product")
                    return
                }
                
                guard let title = product["title"] as? String else {
                    print("error trying to retrieve title of product")
                    return
                }
                
                guard let imageURL = product["image"] as? String else {
                    print("error trying to retrieve image url of product")
                    return
                }
                
                guard let priceData = product["price"] as? [String: AnyObject] else {
                    print("error trying to retrieve price data of product")
                    return
                }
                
                guard let price = priceData["now"] as? String else {
                    print("error trying to retrieve current price of product")
                    return
                }
                
                let attributes = ["productID" : productID,
                                  "price" : price,
                                  "title" : title,
                                  "gridImageURL" : "https:" + imageURL];
                
                let dishwasher = JLTDishwasher.create(with: attributes, managedObjectContext: managedContext!)
                
                dishwashers.append(dishwasher)
            }
            
            do {
                
                try managedContext?.save()
                
            } catch let error as NSError {
                
                print("Could not save.\n    Error Description: \"\(error.localizedDescription)\"\n    Error Info: \"\(error.userInfo)\"\n    Error Object: \"\(error)\"")
                
            }
            
        } catch  {
            print("error trying to convert data to JSON")
            return
        }
    }
    
    func loadDishwasherImages() {
    
        for dishwasher in dishwashers {
            
            loadImage(from: dishwasher.gridImageURL!)
            
        }
        
        dishwasherGridCollectionView.reloadData()
    }
    
    func loadImage(from urlString:String) {
        
        URLSession.downloadImage(with: urlString) { (image) in
            
            OperationQueue.main.addOperation {
                self.gridImages.append(image)
                
                if self.gridImages.count == self.dishwashers.count {
                    
                    self.dishwasherGridCollectionView.reloadData()
                    
                }
                
            }
            
        }
        
    }
    
}
