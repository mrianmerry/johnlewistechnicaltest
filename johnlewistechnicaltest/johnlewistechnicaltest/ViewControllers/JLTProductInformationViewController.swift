//
//  JLTProductInformationViewController.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 10/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//

import UIKit

class JLTProductInformationViewController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var productInformationTableView: UITableView!
    @IBOutlet weak var imageScrollView: UIScrollView!
    @IBOutlet weak var imagePageControl: UIPageControl!
    @IBOutlet weak var imageScrollViewTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var displaySpecialOfferLabel: UILabel!
    @IBOutlet weak var specialOfferHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var specialOfferTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var guaranteeInformationLabel: UILabel!
    
    var dishwasherImages = [UIImage]()
    var dishwasher: JLTDishwasher?
    var productInfoURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let title = dishwasher?.title {
            self.navigationItem.title = title
        }
        
        if let productID = dishwasher?.productID {
            productInfoURL = "https://api.johnlewis.com/v1/products/\(productID)?key=Wu1Xqn3vNrd1p7hqkvB6hEu0G9OrsYGb"
        }
        downloadProductInformationIfNecessary()
        
        setupImageScrollView()
        setupProductInformationLabels()

    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
            let orient = UIApplication.shared.statusBarOrientation
            
            switch orient {
                
            case .portrait:
                break

            default:
                break
            }
            
            self.populateImageScrollView()
            
            let frameHeight = self.imageScrollView.frame.height
            let frameWidth = self.imageScrollView.frame.width
            let visibleImage = CGRect(x: CGFloat(self.imagePageControl.currentPage) * frameWidth,
                                      y: 0, width: frameWidth, height: frameHeight)
            
            self.imageScrollView.scrollRectToVisible(visibleImage, animated: false)
            
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            
        })
        
    }
    
    func downloadProductInformationIfNecessary() {
        
        if dishwasher?.productImageURLs == nil {
            
            if dishwasher != nil {
                
                downloadProductInformation()
            
            }
            
        } else {
            
            loadDishwasherImages()
            
        }
        
    }

    func downloadProductInformation() {
        
        URLSession.createAPIRequest(with: productInfoURL!) { (responseData) in
            
            self.supplementDishwasher(with: responseData)
            self.loadDishwasherImages()
            
            OperationQueue.main.addOperation {

                self.setupProductInformationLabels()
                self.productInformationTableView.reloadData()

            }
            
        }
        
    }
    
    func supplementDishwasher(with data: Data) {
        
        do {
            
            let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
            
            if let media = json?["media"] as? [String: AnyObject] {
                
                if let images = media["images"] as? [String: AnyObject] {
                    
                    if let urls = images["urls"] as? [String] {
                        
                        var imageURLs = [String]()
                        
                        for url in urls {
                            
                            imageURLs.append("https:\(url)")
                        
                        }
                        
                        dishwasher?.productImageURLs = imageURLs
                    }
                    
                }
                
            }
            
            if let details = json?["details"] as? [String: AnyObject] {
                
                if let productInformation = details["productInformation"] as? String {
                    
                    if let attributedInformation = productInformation.attributedStringFromHTML {
                        
                        dishwasher?.productInformation = attributedInformation
                        
                    }
                    
                }
                
                if let features = details["features"] as? [AnyObject] {
                    
                    if let subdetail = features[0] as? [String: AnyObject] {
                        
                        if let attributes = subdetail["attributes"] as? [[String: String]] {
                            
                            var dishwasherSpecification = [[String: String]]()
                            
                            for attribute in attributes {
                                
                                let title = attribute["name"]
                                let value = attribute["value"]
                                
                                if (title != nil) && value != nil {
                                 
                                    let feature = ["title" : title!, "value" : value!]
                                    dishwasherSpecification.append(feature)
                                    
                                }
                                
                            }
                            
                            dishwasher?.productSpecification = dishwasherSpecification
                            
                        }
                        
                    }
                    
                }
                
            }
            
            if let displaySpecialOffer = json?["displaySpecialOffer"] as? String {
                
                dishwasher?.displaySpecialOffer = displaySpecialOffer
                
            }
            
            if let additionalServices = json?["additionalServices"] as? [String: AnyObject] {
                
                if let includedServices = additionalServices["includedServices"] as? [String] {
                    
                    dishwasher?.guaranteeInformation = includedServices[0]
                    
                }
                
            }
            
            if let productCode = json?["code"] as? String {
                
                dishwasher?.productCode = productCode
                
            }
            
            do {
                
                try dishwasher?.managedObjectContext?.save()
                
            } catch {
                
                print("error trying to save managed object context")
                return
                
            }
            
        } catch {
            print("error trying to convert data to JSON")
            return

        }
        
    }

    func setupImageScrollView() {
        
        imageScrollView.delegate = self
        imagePageControl.currentPage = 0
        
        populateImageScrollView()
        
    }
    
    func populateImageScrollView() {
        
        for subview in imageScrollView.subviews {
            
            subview.removeFromSuperview()
            
        }
        
        let scrollViewWidth = imageScrollView.frame.width
        let scrollViewHeight = imageScrollView.frame.height
        let imageCount = dishwasherImages.count
        
        if imageCount == 0 {
            
            return
            
        }
        
        for i in 0...(imageCount - 1) {
            
            let image = dishwasherImages[i]
            let xOrigin = CGFloat(i) * scrollViewWidth
            
            let imageView = UIImageView(frame: CGRect(x: xOrigin, y: 0, width: scrollViewWidth, height: scrollViewHeight))
            
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            
            imageScrollView.addSubview(imageView)
            
        }
        
        imagePageControl.numberOfPages = imageCount
        imageScrollView.contentSize = CGSize(width: CGFloat(imageCount) * scrollViewWidth, height: scrollViewHeight)
        
    }
    
    func loadDishwasherImages() {
        
        for imageURL in (dishwasher?.productImageURLs)! {
            
            loadImage(from: imageURL)
            
        }
        
    }
    
    func loadImage(from urlString:String) {
        
        URLSession.downloadImage(with: urlString) { (image) in
            
            OperationQueue.main.addOperation {
                self.dishwasherImages.append(image)
                
                if self.dishwasherImages.count == self.dishwasher?.productImageURLs?.count {
                    
                    self.populateImageScrollView()
                    
                }
            }
            
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageWidth = imageScrollView.frame.width
        let currentPage = floor((imageScrollView.contentOffset.x - pageWidth/2) / pageWidth) + 1
        
        imagePageControl.currentPage = Int(currentPage)
        
    }
    
    func setupProductInformationLabels() {
        
        priceLabel.text = "£" + (dishwasher?.price)!
        displaySpecialOfferLabel.text = dishwasher?.displaySpecialOffer
        guaranteeInformationLabel.text = dishwasher?.guaranteeInformation
        
        if displaySpecialOfferLabel.text == "" {
            
            specialOfferHeightConstraint.constant = 0
            specialOfferTopConstraint.constant = 0
            
        } else {
            
            specialOfferHeightConstraint.constant = 21
            specialOfferTopConstraint.constant = 8
            
        }
        
    }
    
}

extension JLTProductInformationViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let row = indexPath.row
        
        if row == 0 {
            
            return UITableViewAutomaticDimension
            
        } else if row == 1 {
            
            return 75
            
        } else {
            
            return 60
            
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}

extension JLTProductInformationViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let count = dishwasher?.productSpecification?.count {
            return 2 + count
        }
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var row = indexPath.row
        
        if row == 0 {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "productInformationCell", for: indexPath) as! JLTProductInformationTableViewCell
            
            cell.productInformationLabel.attributedText = dishwasher?.productInformation
            
            return cell
            
        } else if row == 1 {
            
            return tableView.dequeueReusableCell(withIdentifier: "specificationHeader", for: indexPath)
            
        } else {
            
            row -= 2
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "productFeatureCell", for: indexPath) as! JLTProductFeatureTableViewCell
            
            cell.featureTitleLabel.text = dishwasher?.productSpecification?[row]["title"]
            cell.featureValueLabel.text = dishwasher?.productSpecification?[row]["value"]
            
            return cell
            
        }
    }
}
