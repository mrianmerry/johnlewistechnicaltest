//
//  JLTProductInformationTableViewCell.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 11/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//

import UIKit

class JLTProductInformationTableViewCell: UITableViewCell {

    @IBOutlet weak var productInformationLabel: UILabel!

}
