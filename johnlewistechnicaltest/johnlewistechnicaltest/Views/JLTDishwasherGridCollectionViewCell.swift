//
//  JLTDishwasherGridCollectionViewCell.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 07/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//

import UIKit

class JLTDishwasherGridCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var dishwasherImageView: UIImageView!
    @IBOutlet weak var dishwasherTitleLabel: UILabel!
    @IBOutlet weak var dishwasherPriceLabel: UILabel!
}
