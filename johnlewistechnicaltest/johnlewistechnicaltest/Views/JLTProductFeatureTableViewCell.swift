//
//  JLTProductFeatureTableViewCell.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 11/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//

import UIKit

class JLTProductFeatureTableViewCell: UITableViewCell {

    @IBOutlet weak var featureTitleLabel: UILabel!
    @IBOutlet weak var featureValueLabel: UILabel!

}
