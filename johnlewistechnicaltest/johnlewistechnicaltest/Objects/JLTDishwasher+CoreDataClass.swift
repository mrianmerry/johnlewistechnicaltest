//
//  JLTDishwasher+CoreDataClass.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 07/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData

@objc(JLTDishwasher)
public class JLTDishwasher: NSManagedObject {

    class func create(with attributes:[String: String], managedObjectContext:NSManagedObjectContext) -> JLTDishwasher {
        
        let dishwasher = NSManagedObject(entity: JLTDishwasher.entity(), insertInto: managedObjectContext) as! JLTDishwasher
        
        dishwasher.setValuesForKeys(attributes)
        
        return dishwasher
    }
}
