//
//  Extensions.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 10/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//

import Foundation
import UIKit

extension String {
    var attributedStringFromHTML: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        }
        catch {
            print("Error occurred.\n    Error Description: \"\(error.localizedDescription)\"\n    Error: \"\(error)\"")
            return nil
        }
    }
    
    var stringFromHTML: String {
        if let htmlString = attributedStringFromHTML?.string {
            return htmlString
        }
        else {
            return ""
        }
    }
}

extension URLSession {
    
    static func createAPIRequest(with url:String, completion:@escaping (Data)->() ) {
        
        guard let url = URL(string: url) else {
            print("Error: cannot create URL")
            return
        }
        
        let urlRequest = URLRequest(url: url as URL)
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest as URLRequest, completionHandler: { (data, response, error) in
            
            guard error == nil else {
                print("Error: \(error)")
                return
            }
            
            guard let responseData = data else {
                print("Error contacting api.\n    Error Description: \"\(error!.localizedDescription)\"\n    Error Object: \"\(error!)\"")
                return
            }
            
            completion(responseData)
        })
        
        task.resume()
        
    }
    
    static func downloadImage(with urlString:String, completion:@escaping (UIImage)->()) {
        
        let imageURL = URL(string: urlString)!
        
        let session = URLSession(configuration: .default)
        
        let task = session.dataTask(with: imageURL) { (data, response, error) in
            
            if error != nil {
                
                print("Error downloading image.\n    Error Description: \"\(error!.localizedDescription)\"\n    Error Object: \"\(error!)\"")
                
            } else {
                
                if let _ = response as? HTTPURLResponse {
                    
                    if let imageData = data {
                        
                        if let image =  UIImage(data: imageData) {
                            
                            completion(image)
                            
                        } else {
                            
                            print ("Couldn't convert data to image")
                        }
                        
                    } else {
                        
                        print("Couldn't get image: Image is nil")
                        
                    }
                    
                } else {
                    
                    print("Couldn't get response code for some reason")
                    
                }
                
            }
            
        }
        
        task.resume()
        
    }
    
}
