//
//  JLTDishwasher+CoreDataProperties.swift
//  johnlewistechnicaltest
//
//  Created by Ian Merryweather on 07/07/2017.
//  Copyright © 2017 IanMerry. All rights reserved.
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension JLTDishwasher {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<JLTDishwasher> {
        return NSFetchRequest<JLTDishwasher>(entityName: "JLTDishwasher");
    }

    @NSManaged public var displaySpecialOffer: String?
    @NSManaged public var gridImageURL: String?
    @NSManaged public var guaranteeInformation: String?
    @NSManaged public var productCode: String?
    @NSManaged public var productID: String?
    @NSManaged public var productImageURLs: Array<String>?
    @NSManaged public var productInformation: NSAttributedString?
    @NSManaged public var productSpecification: Array<Dictionary<String, String>>?
    @NSManaged public var price: String?
    @NSManaged public var title: String?

}
